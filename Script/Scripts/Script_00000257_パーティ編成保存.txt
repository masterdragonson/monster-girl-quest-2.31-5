class PartyForm
  class Scene_PartyForm < Scene_File
    def create_savefile_windows
      @savefile_windows = Array.new(item_max) do |i|
        Window_PartyForm.new(savefile_height, i)
      end
      @savefile_windows.each { |window| window.viewport = @savefile_viewport }
    end

    def item_max
      3
    end
  end
end

module Vocab
  PartySaveMessage = "Which slot do you want to register a party?"
  PartyLoadMessage = "Which party do you want to call?"
end

class Game_System
  def party_form
    @party_form ||= PartyForm.new
  end
end

class PartyForm
  attr_reader :last_save_index
  def initialize
    @data = {}
    @last_save_index = 0
  end

  def save(index, members)
    @data[index] = members
    @last_save_index = index
  end

  def load(index)
    @data[index]
  end
end

class Scene_PartyLoad < PartyForm::Scene_PartyForm
  def help_window_text
    Vocab::PartyLoadMessage
  end

  def first_savefile_index
    0
  end

  def on_savefile_ok
    super
    members = $game_system.party_form.load(@index)
    if members && members.select { |member| $game_party.exist_all_actor_id?(member) }
      $game_party.members.map(&:id).each do |id|
        $game_party.move_stand_actor(id)
      end
      members.each do |id|
        $game_party.move_actor(id)
      end
      on_save_success
    else
      Sound.play_buzzer
    end
  end

  def on_save_success
    Sound.play_ok
    return_scene
  end
end

class Scene_PartySave < PartyForm::Scene_PartyForm
  def help_window_text
    Vocab::PartySaveMessage
  end

  def first_savefile_index
    $game_system.party_form.last_save_index
  end

  def on_savefile_ok
    super
    if $game_system.party_form.save(@index, $game_party.members.map(&:id))
      on_save_success
    else
      Sound.play_buzzer
    end
  end

  def on_save_success
    Sound.play_ok
    return_scene
  end
end

class PartyForm
  class Window_PartyForm < Window_Base
    attr_reader :selected

    def initialize(height, index)
      super(0, index * height, Graphics.width, height)
      @file_index = index
      refresh
      @selected = false
    end

    def selected=(selected)
      @selected = selected
      update_cursor
    end

    def update_cursor
      if @selected
        cursor_rect.set(0, 0, @name_width + 8, line_height)
      else
        cursor_rect.empty
      end
    end

    def draw_filename(x, y)
      change_color(normal_color)
      name = "Party" + " #{@file_index + 1}"
      draw_text(x, y, 200, line_height, name)
      @name_width = text_size(name).width
    end

    def refresh
      contents.clear
      draw_filename(4, 0)
      draw_party_characters(42, contents.height - line_height - 4)
    end

    def draw_party_characters(x, y)
      members = $game_system.party_form.load(@file_index)
      return unless members

      members.each do |member_id|
        character_name = $game_actors[member_id].character_name
        n = $game_actors[member_id].character_index
        bitmap = Cache.character(character_name)
        sign = character_name[/^[\!\$]./]
        if sign && sign.include?("$")
          cw = bitmap.width / 3
          ch = bitmap.height / 4
        else
          cw = bitmap.width / 12
          ch = bitmap.height / 8
        end
        src_rect = Rect.new((n % 4 * 3 + 1) * cw, (n / 4 * 4) * ch, cw, ch)
        contents.blt(x, y - ch, bitmap, src_rect)
        x += cw
      end
    end
  end
end

